# Astro JS Tutorial

Based on Rodney Lab's [Astro JS Tutorial: Quick Start Astro Guide](https://rodneylab.com/astro-js-tutorial/).

```
pnpm create astro@latest -- --template minimal && cd $_
pnpm astro telemetry disable
pnpm astro add react svelte mdx
pnpm dev
```

[![Open in StackBlitz](https://developer.stackblitz.com/img/open_in_stackblitz.svg)](https://stackblitz.com/github/withastro/astro/tree/latest/examples/minimal)
[![Open with CodeSandbox](https://assets.codesandbox.io/github/button-edit-lime.svg)](https://codesandbox.io/s/github/withastro/astro/tree/latest/examples/minimal)

## 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```
/
├── public/
├── src/
│   └── pages/
│       └── index.astro
└── package.json
```

### Tip for PNPM users

Add `.npmrc` if using `pnpm` with the following content:

```ini
// expose Astro dependencies for 'pnpm' users
shamefully-hoist=true
```

## Notes

- `astro.config.mjs` reminds me very strongly Vite.
- notice hydration in `src/pages/index.astro`:
  ```astro
  <ReactVideo client:load />
  <SvelteVideo client:load />
  ```
- pressing the buttons below the React and Svelte components changes the button and the background colors.

## Fonts

Download the [Roboto fonts](https://google-webfonts-helper.herokuapp.com/api/fonts/roboto?download=zip&subsets=latin&variants=700,regular&formats=woff,woff2), unzip and place the fonts into `public/fonts` folder. Make sure the fonts version, e.g., `v30`, in `src/styles/styles.css` is the same as in the fonts names.

## Hosting

Make sure everything is ready for deployment:

```shell
pnpm build
pnpm preview
```

Add `.nvmrc` file to the project root and specify the LTS version of Node.js, which is 18 at the time of writing. Yet, I set it for 16 because [CloudFlare](https://cloudflare.com) accepts version up to 17 at the time of writing.

> Although we have used `pnpm` in this tutorial to build the site, for maximum compatibility, in the cloud use `npm run build` as your build command.

### CloudFlare

Log in to CloudFlare and create a new `Pages` project connecting it to your Github or Gitlab repository. Configuration is trivial.

### Netlify

Install `netlify cli` globally.

```shell
pnpm i -g netlify-cli
```

Next open the `~/.zshrc` with `nano` and add the `ntl` alias

```bash
alias ntl='netlify'
```

Authorize with Github and create a new Netlify website:

```bash
ntl login # if expired run "ntl logout" first
ntl init # follow the dialog
git push
ntl open
```

## References

- [A nice video which focusses on self-hosted fonts in Astro together with optimisation](https://rodneylab.com/astro-self-hosted-fonts/)
